<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'/v1'], function(){
    Route::post('user/signin', 'UserController@signin');
});

Route::group(['prefix'=>'/v1'], function(){
    Route::resource('user', 'UserController');
    Route::post('user/signup', 'UserController@signup');
    Route::get('users_by_client/{id_client}', 'UserController@usersByClient');

    Route::resource('revenue', 'RevenueController');

    Route::resource('client', 'ClientController');

    Route::resource('responsible', 'ResponsibleController');

    Route::resource('type_therapy', 'TypeTherapyController');

    Route::resource('anamnese', 'AnamneseController');

    Route::resource('medicine', 'MedicineController');

    Route::resource('patient', 'PatientController');
    Route::get('patients_by_client/{id_client}', 'PatientController@pacientsByClient');

    Route::resource('diagnose', 'DiagnoseController');

    Route::resource('disorder', 'DisordeController');

    Route::resource('therapy', 'TherapyController');

    Route::resource('user_has_client', 'UserHasClientController');
    // Route::get('users_by_client/{id_client}', 'UserHasClientController@usersByClient');
});
