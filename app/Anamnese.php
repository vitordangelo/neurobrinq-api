<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anamnese extends Model
{
    public $timestamps = false;
    protected $table = 'anamneses';

    protected $fillable = [
        'description', 'date'
    ];
}
