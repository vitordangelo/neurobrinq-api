<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnose extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'diagnosed_age', 'rate', 'details', 'description', 
        'anamnese_id_anamnese'
    ];
}


