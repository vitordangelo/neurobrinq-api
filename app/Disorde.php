<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disorde extends Model
{
    public $timestamps = false;

    protected $fillable = [
        "rate", "type", "diagnosis_date", "diagnosis_id_diagnosis", 
        "cid10_cid10_id"
    ];
}