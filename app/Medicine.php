<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'dosage', 'date_start', 'date_end_of_use', 'disorders_id_disorders', 
        'disorders_diagnosis_id_diagnosis'
    ];
}