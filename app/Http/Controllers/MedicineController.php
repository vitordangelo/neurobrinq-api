<?php

namespace App\Http\Controllers;

use App\Medicine;
use Illuminate\Http\Request;

class MedicineController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $medicine = Medicine::create($request->all());
        return response()->json([ 'status' => 'Medicamento criado com sucesso', 'code' => 200, 'data' => $medicine], 200);
    }

    public function show($id)
    {
        $medicine = Medicine::find($id);

        if (!$medicine)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Medicamento não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json(['status' => true, 'data' => $medicine], 200);
    }

    public function edit(Medicine $medicine)
    {
        
    }

    public function update(Request $request, $id)
    {
        $medicine = Medicine::find($id);

        if (!$medicine) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Não há registro deste Medicamento', 'error_code' => 404]], 404);
        }

        $medicine->update($request->all());
        return response()->json([ 'status' => 'Medicamento atualizado com sucesso', 'code' => 200, 'data' => $medicine], 200);
    }

    public function destroy($id)
    {
        $medicine = Medicine::destroy($id);

        if (!$medicine) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Não há registro deste Medicamento', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => 'Medicamento removido com sucesso', 'code' => 200, 'data' => $medicine], 200);
    }
}
