<?php

namespace App\Http\Controllers;

use App\Therapy;
use Illuminate\Http\Request;

class TherapyController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $therapy = Therapy::create($request->all());
        return response()->json([ 'status' => 'Tratamento criado com sucesso', 'code' => 200, 'data' => $therapy], 200);
    }

    public function show($id)
    {
        $therapy = Therapy::find($id);

        if (!$therapy)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tratamento não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $therapy], 200);
    }

    public function edit(Therapy $therapy)
    {
        
    }

    public function update(Request $request, $id)
    {
        $therapy = Therapy::find($id);

        if (!$therapy) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tratamento não encontrado', 'error_code' => 404]], 404);
        }

        $therapy->update($request->all());
        return response()->json([ 'status' => true, 'data' => $therapy], 200);
    }

    public function destroy($id)
    {
        $therapy = Therapy::destroy($id);

        if (!$therapy) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tratamento não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $therapy], 200);
    }
}
