<?php

namespace App\Http\Controllers;

use App\Diagnose;
use Illuminate\Http\Request;

class DiagnoseController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $diagnose = Diagnose::create($request->all());
        return response()->json([ 'status' => 'Diagnóstico criado com sucesso', 'code' => 200, 'data' => $diagnose], 200);
    }

    public function show($id)
    {
        $diagnose = Diagnose::find($id);

        if (!$diagnose)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Diagnóstico não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $diagnose], 200);
    }

    public function edit(Diagnose $diagnose)
    {
        
    }

    public function update(Request $request, $id)
    {
        $diagnose = Diagnose::find($id);

        if (!$diagnose) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Diagnóstico não encontrado', 'error_code' => 404]], 404);
        }

        $diagnose->update($request->all());
        return response()->json([ 'status' => true, 'data' => $diagnose], 200);
    }

    public function destroy($id)
    {
        $diagnose = Diagnose::destroy($id);

        if (!$diagnose) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Diagnóstico não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $diagnose], 200);
    }
}
