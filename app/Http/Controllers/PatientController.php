<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use DB;

class PatientController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $patient = Patient::create($request->all());
        return response()->json([ 'status' => 'Paciente criado com sucesso', 'code' => 200, 'data' => $patient], 200);
    }

    public function show($id)
    {
        $patient = Patient::find($id);

        if (!$patient)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Paciente não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $patient], 200);
    }

    public function edit(Patient $patient)
    {
        
    }

    public function update(Request $request, $id)
    {
        $patient = Patient::find($id);

        if (!$patient) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Paciente não encontrado', 'error_code' => 404]], 404);
        }

        $patient->update($request->all());
        return response()->json([ 'status' => true, 'data' => $patient], 200);
    }

    public function destroy($id)
    {
        $patient = Patient::destroy($id);

        if (!$patient) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Paciente não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $patient], 200);
    }

    public function pacientsByClient($idClient) 
    {
        $pacients = DB::table('patients')
            ->where('client_id_client', '=', $idClient)
            ->select('*')
            ->get();

        if (count($pacients) == 0) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Cliente não possuí paciente cadastrado', 'error_code' => 404]], 404);
        }
        
        return response()->json([ 'status' => true, 'data' => $pacients], 200);
    }
}
