<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    public function signup(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'data_acess_user' => 'required',
        ]);

        DB::table('users')->insert([
            'type' => $request->input('type'),
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'data_acess_user' => $request->input('data_acess_user'),
            'skype' => $request->input('skype'),
            'client_id' => $request->input('client_id'),
        ]);

        return response()->json([
            'status' => 'Usuário criado com sucesso',
            'code' => 200,
        ], 200);
    }

    public function signin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
            
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Could not create token'], 500);
        }

        $user = DB::table('users')
            ->where('email', '=', $request->email)
            ->select('type', 'name', 'phone', 'email', 'skype', 'data_acess_user')
            ->get();

        return response()->json(['token' => $token, 'user' => $user], 200);
    }
    
    public function index()
    {
        
    }

   
    public function create()
    {
    
    }

    
    public function store(Request $request)
    {
        
    }

    
    public function show(User $user)
    {
        
    }

   
    public function edit(User $user)
    {
        
    }

    
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['error' => 'Não há registro deste usuário'], 404);
        }

        $user->update($request->all());

        return response()->json([ 'status' => 200, 'data' => $user], 200);

        // $user = DB::table('users')
        //     ->where('id_user', $id)
        //     ->update([
        //         'type' => $request->input('type'),
        //         'name' => $request->input('name'),
        //         'phone' => $request->input('phone'),
        //         'email' => $request->input('email'),
        //         'password' => bcrypt($request->input('password')),
        //         'data_acess_user' => $request->input('data_acess_user'),
        //         'skype' => $request->input('skype'),
        //     ]);

        // if (!$user) {
        //     return response()->json(['error' => 'Não há registro deste motorista'], 404);
        // }

        // return response()->json([ 'status' => 200, 'data' => $user], 200);
    }

    
    public function destroy($id)
    {
        $user = DB::table('users')->where('id_user', '=', $id)->delete();

        if (!$user) {
            return response()->json(['error' => 'Não tem nenhum usuário com este ID'], 404);
        }

        return response()->json([ 'status' => 200, 'data' => $user], 200);
    }

    public function usersByClient($idClient)
    {
        $users = DB::table('users')
            ->where('client_id', '=', $idClient)
            ->select('id', 'type', 'name', 'phone', 'email', 'skype', 'data_acess_user', 'client_id')
            ->get();

        if (count($users) == 0) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Cliente não possuí usuário cadastrado', 'error_code' => 404]], 404);
        }
        
        return response()->json([ 'status' => true, 'data' => $users], 200);
    }
}
