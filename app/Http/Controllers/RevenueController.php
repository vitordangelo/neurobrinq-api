<?php

namespace App\Http\Controllers;

use App\Revenue;
use Illuminate\Http\Request;

class RevenueController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {   
        $revenue = Revenue::create($request->all());
        return response()->json([ 'status' => 'Revenue criada com sucesso', 'code' => 200, 'data' => $revenue], 200);
    }

    public function show($id)
    {
        $revenue = Revenue::find($id);

        if (!$revenue)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Revenue não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $revenue], 200);
    }

    public function edit(Revenue $revenue)
    {
        
    }

    public function update(Request $request, $id)
    {
        $revenue = Revenue::find($id);

        if (!$revenue) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Revenue não encontrado', 'error_code' => 404]], 404);
        }

        $revenue->update($request->all());
        return response()->json([ 'status' => true, 'data' => $revenue], 200);
    }

    public function destroy($id)
    {
        $revenue = Revenue::destroy($id);

        if (!$revenue) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Revenue não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $revenue], 200);
    }
}
