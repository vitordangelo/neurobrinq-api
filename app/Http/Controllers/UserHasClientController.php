<?php

namespace App\Http\Controllers;

use App\UserHasClient;
use Illuminate\Http\Request;
use DB;

class UserHasClientController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $user_has_client = UserHasClient::create($request->all());
        return response()->json([ 'status' => 'Usuário do Cliente criado com sucesso', 'code' => 200, 'data' => $user_has_client], 200);
    }

    public function show($id)
    {
        $user_has_client = UserHasClient::find($id);

        if (!$user_has_client)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Usuário não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $user_has_client], 200);
    }

    public function edit(UserHasClient $user_has_client)
    {
        
    }

    public function update(Request $request, $id)
    {
        $user_has_client = UserHasClient::find($id);

        if (!$user_has_client) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Usuário não encontrado', 'error_code' => 404]], 404);
        }

        $user_has_client->update($request->all());
        return response()->json([ 'status' => true, 'data' => $user_has_client], 200);
    }

    public function destroy($id)
    {
        $user_has_client = UserHasClient::destroy($id);

        if (!$user_has_client) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Usuário não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $user_has_client], 200);
    }

    public function usersByClient($idClient)
    {
        $users = DB::table('user_has_client')
            ->join('users', 'users.id', '=', 'user_has_client.user_id_user')
            ->where('client_id_client', '=', $idClient)
            ->select('users.id', 'users.data_acess_user', 'users.email',
                'users.name', 'users.phone', 'users.skype', 'users.type'
            )
            ->get();
        
        if (count($users) == 0) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Paciente não possuí usuário cadastrado', 'error_code' => 404]], 404);
        }
        
        return response()->json([ 'status' => true, 'data' => $users], 200);
    }
}
