<?php

namespace App\Http\Controllers;

use App\TypeTherapy;
use Illuminate\Http\Request;

class TypeTherapyController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $typeTherapy = TypeTherapy::create($request->all());
        return response()->json([ 'status' => 'Tipo de Terapia criado com sucesso', 'code' => 200, 'data' => $typeTherapy], 200);
    }

    public function show($id)
    {
        $typeTherapy = TypeTherapy::find($id);

        if (!$typeTherapy)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tipo de Terapia não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $typeTherapy], 200);
    }

    public function edit(TypeTherapy $typeTherapy)
    {
        
    }

    public function update(Request $request, $id)
    {
        $typeTherapy = TypeTherapy::find($id);

        if (!$typeTherapy) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tipo de Terapia não encontrado', 'error_code' => 404]], 404);
        }

        $typeTherapy->update($request->all());
        return response()->json([ 'status' => true, 'data' => $typeTherapy], 200);
    }

    public function destroy($id)
    {
        $typeTherapy = TypeTherapy::destroy($id);

        if (!$typeTherapy) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Tipo de Terapia não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $typeTherapy], 200);
    }
}
