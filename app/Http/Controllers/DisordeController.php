<?php

namespace App\Http\Controllers;

use App\Disorde;
use Illuminate\Http\Request;

class DisordeController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $disorde = Disorde::create($request->all());
        return response()->json([ 'status' => 'Enfermidade criado com sucesso', 'code' => 200, 'data' => $disorde], 200);
    }

    public function show($id)
    {
        $disorde = Disorde::find($id);

        if (!$disorde)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Enfermidade não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $disorde], 200);
    }

    public function edit(Disorde $disorde)
    {
        
    }

    public function update(Request $request, $id)
    {
        $disorde = Disorde::find($id);

        if (!$disorde) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Enfermidade não encontrado', 'error_code' => 404]], 404);
        }

        $disorde->update($request->all());
        return response()->json([ 'status' => true, 'data' => $disorde], 200);
    }

    public function destroy($id)
    {
        $disorde = Disorde::destroy($id);

        if (!$disorde) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Enfermidade não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $disorde], 200);
    }
}
