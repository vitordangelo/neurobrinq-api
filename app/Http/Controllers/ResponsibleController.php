<?php

namespace App\Http\Controllers;

use App\Responsible;
use Illuminate\Http\Request;

class ResponsibleController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $responsible = Responsible::create($request->all());
        return response()->json([ 'status' => 'Responsável criado com sucesso', 'code' => 200, 'data' => $responsible], 200);
    }

    public function show($id)
    {
        $responsible = Responsible::find($id);

        if (!$responsible)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Responsável não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $responsible], 200);
    }

    public function edit(Responsible $responsible)
    {
        
    }

    public function update(Request $request, $id)
    {
        $responsible = Responsible::find($id);

        if (!$responsible) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Responsável não encontrado', 'error_code' => 404]], 404);
        }

        $responsible->update($request->all());
        return response()->json([ 'status' => true, 'data' => $responsible], 200);
    }

    public function destroy($id)
    {
        $responsible = Responsible::destroy($id);

        if (!$responsible) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Responsável não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $responsible], 200);
    }
}
