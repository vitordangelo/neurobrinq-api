<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::all();

        if (!$clients)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Não existe cliente cadastrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $clients], 200);
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $client = Client::create($request->all());
        return response()->json([ 'status' => 'Cliente criado com sucesso', 'code' => 200, 'data' => $client], 200);
    }

    public function show($id)
    {
        $client = Client::find($id);

        if (!$client)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Cliente não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $client], 200);
    }

    public function edit(Client $client)
    {
        
    }

    public function update(Request $request, $id)
    {
        $client = Client::find($id);

        if (!$client) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Cliente não encontrado', 'error_code' => 404]], 404);
        }

        $client->update($request->all());
        return response()->json([ 'status' => true, 'data' => $client], 200);
    }

    public function destroy($id)
    {
        $client = Client::destroy($id);

        if (!$client) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Cliente não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $client], 200); 
    }
}
