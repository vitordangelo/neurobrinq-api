<?php

namespace App\Http\Controllers;

use App\Anamnese;
use Illuminate\Http\Request;

class AnamneseController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $anamnese = Anamnese::create($request->all());
        return response()->json([ 'status' => 'Anamnese criado com sucesso', 'code' => 200, 'data' => $anamnese], 200);
    }

    public function show($id)
    {
        $anamnese = Anamnese::find($id);

        if (!$anamnese)
        {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Anamnese não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $anamnese], 200);
    }

    public function edit(Anamnese $anamnese)
    {
        
    }

    public function update(Request $request, $id)
    {
        $anamnese = Anamnese::find($id);

        if (!$anamnese) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Anamnese não encontrado', 'error_code' => 404]], 404);
        }

        $anamnese->update($request->all());
        return response()->json([ 'status' => true, 'data' => $anamnese], 200);
    }

    public function destroy($id)
    {
        $anamnese = Anamnese::destroy($id);

        if (!$anamnese) {
            return response()->json(['status' => false, 'error ' => ['error_message' => 'Anamnese não encontrado', 'error_code' => 404]], 404);
        }

        return response()->json([ 'status' => true, 'data' => $anamnese], 200);
    }
}
