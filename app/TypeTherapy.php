<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTherapy extends Model
{
    public $timestamps = false;
    protected $table = 'type_therapy';

    protected $fillable = [
        'description'
    ];
}
