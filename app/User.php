<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;

    protected $fillable = [
        'type', 'name', 'phone', 'email', 'skype', 'data_acess_user', 'password', 'client_id',
    ];

    protected $hidden = [
        'password'
    ];
}
