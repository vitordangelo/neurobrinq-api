<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Therapy extends Model
{
    public $timestamps = false;
    protected $table = 'therapies';

    protected $fillable = [
        'type', 'date_start', 'date_termination_forecast', 'disorders_id_disorders', 
        'disorders_diagnosis_id_diagnosis', 'type_therapy_id_type_therapy'
    ];
}