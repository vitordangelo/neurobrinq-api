<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name_clinic', 'address_city', 'address_cep', 'address_street', 
        'address_number', 'address_neighborhood', 'address_complement', 
        'cellphone', 'landline', 'email', 'site', 'host_acess', 'bank_agency', 
        'bank_account', 'type', 'social_name', 'cnpj',
        'ie', 'address', 'address_state'
    ];

}