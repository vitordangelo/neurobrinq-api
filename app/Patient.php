<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'date_of_birth', 'genre', 'deficiency', 'allergy', 
        'address_city', 'address_street', 'address_residence_number', 
        'address_neighborhood', 'address_complement', 'client_id_client', 
        'diagnosis_id_diagnosis'    
    ];
}


