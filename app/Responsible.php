<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'name', 'email', 'cpf', 'rg', 'address_city', 
        'address_street', 'address_cep', 'address_residence_number', 
        'address_neighborhood', 'address_complement', 'date_of_birth', 
        'genre', 'nationality', 'marital_status', 'profession', 
        'patient_id_pacient', 'patient_client_id_client'
    ];
}


