<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasClient extends Model
{
    public $timestamps = false;
    protected $table = 'user_has_client';

    protected $fillable = [
        "user_id_user", "client_id_client"
    ];
}

